const MongoClient = require('mongodb').MongoClient;

const collectionName = 'users';

async function getDbConnection(databaseName, collectionName) {
    const db = (await MongoClient.connect(`mongodb://localhost:27017`, {useNewUrlParser: true}))
        .db(databaseName);
    if (collectionName) {
        return await db.createCollection(collectionName);
    } else {
        return db;
    }
}

class UserDatabase{
    constructor(skillName) {
        const self = this;
        return new Promise(async (resolve) => {
            const db = await getDbConnection(skillName);
            self.userDb = await db.collection(collectionName);
            resolve(self);
        })
    }


    async readData(userId, fields) {
        console.log(`read db request, userId: ${userId}, fields: ${(fields) ? fields: 'not specified'}`);
        const startTime = Date.now();
        const projection = {_id: false};
        if (fields) {
            fields.forEach(field => projection.field = true)
        }
        const userData = await this.userDb.findOne(
            {_id: userId},
            projection
            );
        console.log(`request took ${(Date.now() - startTime) / 1000} sec.`);
        return userData;
    }

    async readPost(userId, fields) {
        console.log(`read db request, userId: ${userId}, fields: ${(fields) ? fields: 'not specified'}`);
        const startTime = Date.now();
        const projection = {_id: false};
        if (fields) {
            fields.forEach(field => projection.field = true)
        }
        const userData = await this.userDb.findOne(
            {_id: userId},
            projection
        );
        console.log(`request took ${(Date.now() - startTime) / 1000} sec.`);
        return userData;
    }

    async writeData(userId, data){
        const startTime = Date.now();
        console.log(`writing db request, userId: ${userId}, data: ${Object.keys(data)}`);
        const status = await this.userDb.updateOne(
            {_id: userId},
            {$set: data},
            {upsert: true});
        console.log(`request took ${(Date.now() - startTime) / 1000} sec.`);
        return status.acknowledged;
    }
}

module.exports = UserDatabase;
