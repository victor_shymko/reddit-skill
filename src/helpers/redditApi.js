const request = require('request-promise');
const fs = require('fs');

const apiUrl = 'https://oauth.reddit.com';

class RedditApi{
    constructor(skillId) {
        this.userAgent = `Alexa skill: ${skillId}/v 0.1 by Victor Shymko`;
    }

    async getLatestPosts(accessToken, sorting, feed) {

        console.log(`sorting by ${sorting}, feed: ${feed}`);

        const options = {
            method: 'GET',
            headers: {
                'User-Agent': this.userAgent,
                Authorization: `bearer ${accessToken}`
            },
        };
        feed = (feed === 'my') ? null: feed;

        const sub = (feed) ?
            `${(feed !== 'original') ? '/r/': "/"}${feed}`:
            "";

        sorting = (sorting) ? `/${sorting}`: ``;

        options.url = `${apiUrl}${sub}${sorting}?limit=50`;

        console.log(options.url);

        try {
            const data = JSON.parse(await request(options));
            console.log(`${data.data.children.length} posts received`);
            //fs.writeFileSync('new.json', JSON.stringify(data.data.children), 'utf-8');
            //console.log(`response from reddit received!`);
            return data.data.children;
        } catch (e) {
            console.log(e.stack);
            return {};
        }
    }

    async getCommentaries(accessToken, link) {
        /*const linkParts = link.split('/');
        link = linkParts.slice(0, linkParts.length - 2).join('/');
        console.log(`link is: ${link}`);*/

        const options = {
            method: 'GET',
            headers: {
                'User-Agent': this.userAgent,
                Authorization: `bearer ${accessToken}`
            },
            url: `${apiUrl}${link}`
        };

        try{
            const resp = await request(options);
            const parsedResp = JSON.parse(resp);
            //fs.writeFileSync('comments.json', JSON.stringify(parsedResp), 'utf-8');
            const topComments = parsedResp[1].data.children
                .map(comment => {
                return {
                    author: comment.data.author,
                    text: comment.data.body
                }
            });
            console.log(`comments length: ${topComments.length}`);
            return topComments;
        } catch (e) {
            console.log(e.stack);
            return [];
        }
    }
}

async function wait(millisecons) {
    return new Promise(resolve => {
        setTimeout(resolve, millisecons);
    })
}

module.exports = RedditApi;
