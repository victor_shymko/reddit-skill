const Clarifai = require('clarifai');

const apiKey = process.env.CLARIFAI_API_KEY;

const app = new Clarifai.App({
    apiKey: apiKey
});

async function describePicture (url) {
    try {
        const startTime = Date.now();
        const response = await app.models.predict(Clarifai.GENERAL_MODEL, url);
        const concepts = response['outputs'][0]['data']['concepts'];
        console.log(`Processing took: ${Date.now() - startTime}ms`);
        return concepts.slice(0, 7).map(concept => concept.name);
    } catch (e) {
        console.log(e.stack);
        return [];
    }
}

module.exports = describePicture;
