const franc = require('franc-min');
const unicode = require('unicode-regex');
const isLetter = unicode({ General_Category: ['Letter'] }).toRegExp();
const urlRegex = /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/igm;
const cleanTextRegex = /http:\/\/|https:\/\/|&amp;|#x200B;|&gt;|&lt;/g;

function isEnglishCharacter(character) {
    const code = character.toLowerCase().charCodeAt(0);
    return code >= 97 && code <= 122;
}

function isEnglishWord(word) {
    for (let ind = 0; ind < word.length; ind++) {
        const ch = word.charAt(ind);

        if (isLetter.test(ch) && !isEnglishCharacter(ch)) {
            return false;
        }
    }
    return true;
}

module.exports.isEnglish = function (title) {
    title = title
        .replace(urlRegex, " ")
        .replace(cleanTextRegex, " ")
        .replace(/\s+/g, " ");

    if (franc(title) === 'eng') {
        console.log('franc checking OK');
        return true;
    }

    let engWordCount = 0;
    const words = title.split(" ");
    for (let word of words) {
        if (isEnglishWord(word)) engWordCount++
    }

    console.log(words, words.length, engWordCount);
    return engWordCount >= words.length / 2;
};

module.exports.cleanText = function (text) {
    // console.log(`was: ${text}`);
    text = text
        .replace(cleanTextRegex, " ")
        .replace(/\s+/g, " ");

    const words = text.split(" ");
    const filteredWords = [];
    for (let word of words) {
        if (isEnglishWord(word))
            filteredWords.push(word);
    }

    const cleanText = filteredWords.join(' ');
    // console.log(`become: ${cleanText}\n`);
    return cleanText;
};