const extractor = require('unfluff');
const request = require('request-promise');

async function getArticleData (link) {
    const data = await request.get(link);
    return extractor(data);
}

module.exports = getArticleData;
