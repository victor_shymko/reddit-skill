// ------------------------------------------------------------------
// APP CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
    logging: false,

    intentMap: {
        'AMAZON.HelpIntent': 'Help',
        'AMAZON.NavigateHomeIntent': 'GoHome',
        'AMAZON.YesIntent': 'Yes',
        'AMAZON.NoIntent': 'No',
        'Default Welcome Intent': 'LAUNCH',
        'Default Fallback Intent': 'Unhandled'
    },

    intentsToSkipUnhandled: ['AMAZON.CancelIntent', 'AMAZON.StopIntent', 'GoHome'],
 };
