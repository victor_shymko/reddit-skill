'use strict';

const { ExpressJS, Lambda } = require('jovo-framework');
const { app } = require ('./app.js');
const ejs = require('ejs');
const path = require('path');
const request = require('request-promise');
const bodyParser = require('body-parser');
const querystring = require('querystring');
const verifier = require('alexa-verifier-middleware');
const express = require('express');
const Webhook = express();
Webhook.set('view engine', 'ejs');

// ------------------------------------------------------------------
// HOST CONFIGURATION
// ------------------------------------------------------------------
const prodWebhookUrl = process.env.WEBHOOK_URL;
const prodWebhookPort = process.env.WEBHOOK_PORT || 3500;
const requestsData = {};
const proxyDomain = (process.platform === 'win32') ?
    'https://9dfb40e8.ngrok.io':
    prodWebhookUrl;

// ExpressJS (Jovo Webhook)
if (process.argv.indexOf('--webhook') > -1) {
    let port = (process.platform === 'win32') ? 3000 : prodWebhookPort;
    console.log(`port type was: ${typeof port}`);
    port = (typeof port === "string") ? parseInt(port) : port;
    console.log(`port type become: ${typeof port}`);
    Webhook.jovoApp = app;

    app.on('ready', () => {
        Webhook.listen(port, function () {
            console.info(`Local server listening on port ${port}.`);
        });
    });

    Webhook.get('/check/', async (req, res) => {
        res.sendStatus(200);
    });

    Webhook.post('/reddit-alexa/', verifier, bodyParser.json(), async (req, res) => {
        console.log('alexa webhook call');
        await app.handle(new ExpressJS(req, res));
    });

    Webhook.post('/reddit-google/', bodyParser.json(), async (req, res) => {
        console.log('google webhook call');
        await app.handle(new ExpressJS(req, res));
    });

    Webhook.get('/reddit-auth/', (req, res) => {

        const authUrl = "https://www.reddit.com/api/v1/authorize.compact";
        const redirectUrl = `${proxyDomain}/reddit-redirect/`;

        // saving current redirect url to
        requestsData[req.query.state] =  req.query.redirect_uri;
        setTimeout(() => {
            console.log(`deleting state ${req.query.state.slice(0, 20)}`);
            delete requestsData[req.query.state];
        }, 1000 * 60 * 10);
        console.log('here is request data: ');
        console.log(requestsData);

        // replacing redirect uri by own
        const urlQuery = req.query;
        urlQuery.redirect_uri = redirectUrl;
        urlQuery.duration = 'permanent';
        delete urlQuery.user_locale;
        console.log(urlQuery);

        // encoding auth url
        const authLink = `${authUrl}?` + querystring.stringify(urlQuery);
        console.log(authLink);
        res.redirect(authLink);
        // res.render(path.join(__dirname + '/getStarted.ejs'), {authLink});
    });

    Webhook.get('/reddit-redirect/', (req, res) => {
        //receiving redirect request and sending it to amazon
        let redirectUrl = requestsData[req.query.state];

        if (redirectUrl) {
            delete requestsData[req.query.state];
        }
        console.log(`redirect url is: ${redirectUrl}`);
        if (!redirectUrl) {
            console.log(`rendering failure`);
            res.render(path.join(__dirname + '/web-pages/lastStep.ejs'));
        } else {
            console.log(`rendering redirect`);
            const fullRedirectUrl = `${redirectUrl}?code=${req.query.code}&state=${req.query.state}`;
            console.log(fullRedirectUrl);
            res.redirect(fullRedirectUrl);
        }
    });

    Webhook.post('/reddit-token/', bodyParser.urlencoded({extended: false}), async (req, res) => {
        const formData = req.body;
        formData.redirect_uri = `${proxyDomain}/reddit-redirect/`;
        console.log(formData);
        const tokenUrl = `https://www.reddit.com/api/v1/access_token`;
        let authHeader = req.get('Authorization');
        if (!authHeader) {
            const token = Buffer.from(`${formData.client_id}:${formData.client_secret}`).toString('base64');
            authHeader = `Basic ${token}`;
            delete formData.client_secret;
        }
        const options = {
            url: tokenUrl,
            method: 'POST',
            headers: {
                Authorization: authHeader,
                'Content-Type' : 'application/x-www-form-urlencoded'
            },
            form: formData
        };
        console.log(req.headers);
        console.log(options);
        let response;
        try{
            response = JSON.parse(await request(options));
        } catch (e) {
            console.log(e.stack);
            response = {};
        }
        console.log(response);
        res.json(response);
    });

    Webhook.get('/reddit-about/', (req, res)=> {
        res.render(path.join(__dirname + `/web-pages/about.ejs`));
    });

    Webhook.get('/reddit-policy/', (req, res)=> {
        res.render(path.join(__dirname + `/web-pages/policy.ejs`));
    });

    Webhook.get('/reddit-terms/', (req, res)=> {
        res.render(path.join(__dirname + `/web-pages/terms.ejs`));
    });
}

// AWS Lambda
exports.handler = async (event, context, callback) => {
    await app.handle(new Lambda(event, context, callback));
};