'use strict';

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const { App } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { GoogleAssistant } = require('jovo-platform-googleassistant');
const { JovoDebugger } = require('jovo-plugin-debugger');
const RedditApiClass = require('./helpers/redditApi.js');
const redditApi = new RedditApiClass('News from Reddit');
const {isEnglish, cleanText} = require('./helpers/text-processing');
const app = new App();
const request = require('request-promise');
const extractor = require('unfluff');
const describePicture = require('./helpers/describePicture');
const UserDatabase = require('./helpers/userDatabase');


app.use(
    new GoogleAssistant(),
    new Alexa(),
    new JovoDebugger(),
);


let userDb;
new UserDatabase('reddit-news')
    .then((db) => {
        userDb = db;
        app.emit('ready');
    });

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------
function hasAccess() {
    const accessToken = this.getAccessToken();
    console.log(`access token: ${accessToken}`);
    if (!accessToken) {
        if (this.getType() === 'AlexaSkill') {
            this.$alexaSkill.showAccountLinkingCard();
        } else {
            this.$googleAction.showAccountLinkingCard();
        }
        const isAlexa = this.getType() === 'AlexaSkill';
        this.tell(`I've send some information to your ${isAlexa ? 'Alexa' : 'Google'} app. Open the app and link your account.`);
    }
    return !!accessToken;
}

const titlesSpeechEnding = "Would you like to listen to 5 more posts?";

function generateSsml(posts, sorting, index, subreddit) {
    const isAlexa = this.getType() === 'AlexaSkill';
    const msForCh = (isAlexa) ? 67.5: 56;
    const moreSpeech = (index === 0) ? "": "more";
    const platformInvocation = (isAlexa) ? 'Alexa': 'OK Google';
    const hintSpeech = (index === 0) ? `you can listen to post details saying '${platformInvocation}, open', just after post title:`: "";
    const subredditSpeech = (isAlexa) ?
         (subreddit) ? `from <emphasis>${subreddit}</emphasis> subreddit`: "":
        (subreddit) ? `from <break time="100ms"/>${subreddit}<break time="50ms"/>`: "";

    const startSpeech = `Here is 5 ${moreSpeech} ${sorting} posts ${subredditSpeech}, ${hintSpeech}`;
    const tagsLength = (subreddit) ? (isAlexa) ? 21: 30: 0;
    const startSpeechLength = startSpeech.length - tagsLength;
    let time = (msForCh * startSpeechLength) + 500;
    console.log(time);

    const speechParts = ['<speak>', startSpeech, '<break time="500ms"/>'];
    for (let post of posts) {
        const interPostInterval = 2000;
        let subredditSpeech = `post with ${post.type} `;
        subredditSpeech +=
            (!subreddit)?
                (isAlexa) ?
                    `from <emphasis>${post.subreddit}</emphasis> subreddit` :
                    `from <break time="80ms"/>${post.subreddit}<break time="40ms"/> subreddit`
                : ``;
        //console.log(subredditSpeech);
        subredditSpeech += `, by ${post.author}: `;
        const titleSpeech = cleanText(post.title);

        speechParts.push(
            subredditSpeech,
            (isAlexa) ? `<voice name="Joanna">`: `<prosody pitch="+15%">`,
            titleSpeech,
            (isAlexa) ? `</voice>`: `</prosody>`,
            `<break time="${interPostInterval}ms"/>`
        );

        const tagsLength = (subreddit) ? (isAlexa) ? 21: 40: 0;
        const beforeTitleSpeechLength = subredditSpeech.length - tagsLength;
        time += (msForCh * (titleSpeech.length + beforeTitleSpeechLength)) + interPostInterval;
        console.log(time);
        post.time = time + 7000;
    }
    speechParts.push(titlesSpeechEnding);
    speechParts.push(`</speak>`);
    return speechParts.join('\n');
}

function getPostType(post) {
    let hint = post.data.post_hint;
    const typesTable = {
        'hosted:video': 'video',
        'rich:video': 'video',
        'link': 'external link',
        'image': 'picture',
        'self': 'text'
    };
    console.log(hint);
    return (typesTable[hint]) ? typesTable[hint]: 'text';
}


function prepeareTitlesResponse(apiData, index) {
    const result = [];
    for (let ind = index; result.length < 5; ind++) {

        if (ind === apiData.length) return null;
        console.log(`title: ${apiData[ind].data.title}, is English: ${isEnglish(apiData[ind].data.title)}`);
        if (!isEnglish(apiData[ind].data.title)) {
            continue;
        }

        // finding type of post
        const type = getPostType(apiData[ind]);

        // prepearing post text
        const titleText = cleanText(apiData[ind].data.title);


        result.push({
            index: ind,
            type: type,
            title: titleText,
            author: apiData[ind].data.author,
            subreddit: apiData[ind].data.subreddit
        });
    }
    //console.log(result);

    return result;
}


function getTimeDifference() {
    return `time difference is: ${Date.now() - Date.parse(this.getTimestamp())}ms`;
}


async function getFeedData(sort, feed) {
    let posts = await redditApi.getLatestPosts(this.getAccessToken(), sort, feed);
    const defaultSortMap = {
        null: 'best',
        my: 'best',
        all: 'hot',
        popular: 'hot',
        original: 'hot'
    };
    const sorting = (sort) ? sort : defaultSortMap[feed];

    await userDb.writeData(this.getUserId(), {
        posts: posts,
        index: 0,
        sorting: (sorting) ? sorting: 'best'
    });
}

async function readFeedTitles(nextPostInd) {
    let {posts, sorting, index} = await userDb.readData(this.getUserId());
    if (!nextPostInd) nextPostInd = index;
    const prepearedPosts = prepeareTitlesResponse(posts, nextPostInd);
    if (!prepearedPosts) {
        this.ask(`No more posts left to read. You can say 'go home' to continue.`,
            `Say 'go home' to continue`);
        return;
    }
    const ssml = generateSsml.call(this, prepearedPosts, sorting, nextPostInd);
    //console.log(ssml);
    userDb.writeData(this.getUserId(), {index: nextPostInd, startTime: Date.now()});
    this.$session.$data.posts = prepearedPosts.map(post => {return {
        time: post.time,
        index: post.index
    }});
    this.followUpState('ReadingPostsState').ask(ssml, titlesSpeechEnding);
}

const returnToFeedSpeech = `You can say 'read comments' or 'back to feed' to continue.`;
const amazonResponeLength = 5000;
const longArticleReprompt = `Would you like to continue listening`;

async function getTextPost(text, title) {
    if (text.trim().length === 0) text = `There is no text in this post.`;
    console.log(`text length: ${text.length}`);
    let speech;
    let reprompt;
    if (text.length > amazonResponeLength) {
        const splitIndex = text.slice(0, amazonResponeLength).lastIndexOf(' ');
        const readingPart = text.slice(0, splitIndex);
        userDb.writeData(this.getUserId(), {reading: text.slice(splitIndex + 1)});
        this.followUpState('ReadingPostsState.LongArticleState');
        speech = `${(this.$session.$data.nonFirst) ? `${title}. ${readingPart}` : readingPart}${(readingPart.endsWith('.')) ? '' : '.'} ${longArticleReprompt}`;
        console.log(speech);
        reprompt = longArticleReprompt;

    } else {
        this.followUpState('ReadingPostsState.OpenedPostState');
        speech = `${title}. ${text}. ${returnToFeedSpeech}`;
        reprompt = returnToFeedSpeech;
    }
    return {
        speech: speech,
        reprompt: reprompt
    }
}

/**
 * @param post data
 * @return {Promise<void>}
 */
async function readPost(post) {
    const type = getPostType(post);
    this.followUpState('ReadingPostsState.OpenedPostState');

    let speechData = {};
    if (type === 'text' || type === 'external link') {
        let textBody;
        if (type === 'text') {
            textBody = post.data.selftext;
        } else {
            try {
                console.log(`getting external link: ${post.data.url}`);
                const rawHtml = await request.get(post.data.url);
                textBody = extractor(rawHtml).text;
            } catch (e) {
                textBody = "Sorry, i can't read the link text.";
                console.log(e.stack);
            }
        }
        textBody = cleanText(textBody);
        speechData = await getTextPost.call(this, textBody, post.data.title);
        if (speechData.speech.length === 0) {
            speechData.speech = `There's no text in this post. `;
        }
        console.log(textBody);
    } else {
        let speech;
        if (type === 'picture') {
            const description = await describePicture(post.data.url);
            console.log(description);
            speech = (description.length > 0) ?
                `I can describe this picture using next words: ${description.join(', ')}. `:
                `I can't show or even describe this picture at the moment. `
        }
        else if (type === 'video'){
            let description;
            try {
                console.log(`getting video description: ${post.data.url}`);
                const rawHtml = await request({url: post.data.url, timeout: 7000});
                description = extractor(rawHtml).text.slice(0, 7500);
            } catch (e) {
                description = "";
                console.log(e.stack);
            }
            speech = `This is video from ${post.data.domain} domain. `;
            console.log(`author : `);
            console.log(post.data.media.oembed);
            if (post.data.media.oembed && post.data.media.oembed.author_name) {
                speech += `Author - ${post.data.media.oembed.author_name}. `
            }
            speech += `I can't show the video at the moment. `;
            if (description.length !== 0) {
                speech += `Here is video description: ${cleanText(description)}. `
            }
        }
        speech += returnToFeedSpeech;
        speechData.speech = speech;
        speechData.reprompt = returnToFeedSpeech;
    }
    console.log(speechData.speech.length, speechData.speech.slice(0, 20));
    this.ask(speechData.speech, speechData.reprompt);
}

const unhandledSpeech = `I didn't get what you just sad. Repeat your command or say 'help' to continue.`;
const unhandledSpeechReprompt = `You can repeat your command or say 'help', to continue.`;

const defaultUserData = {
    userId: "",
    posts: [],
    startTime: 0
};

function getRandomElem (list) {
    return list[Math.floor(Math.random()*list.length)];
}

app.setHandler({
    async LAUNCH() {
        if (!hasAccess.call(this)) {return;}
        console.log(`session id: ${this.$request.session.sessionId}`);
        const isNew = await userDb.readData(this.getUserId(), ['index']);
        const isAlexa = this.getType() === 'AlexaSkill';
        const simpleInvocation = (isAlexa) ? 'read my feed': 'read feed';
        const skillName = (isAlexa) ? `News reader for Reddit`: 'Reddit feed';
        if (!isNew) {
            this.ask(`Welcome to ${skillName}, this skill allows you to listen to your personal feed from reddit.
For reading your feed just say, ${simpleInvocation}. Also you can define post sorting, 
by saying, read new posts, 
define feed name by saying, 'read popular feed',  
define both sorting and feed name by saying, 'read new posts from popular feed'. 
Now you can say your command.`,
                `You can say, '${simpleInvocation}'`);
            return;
        }

        let speech = `You can say, '${simpleInvocation}' or 'help', to continue`;
        const reprompt = `Say '${simpleInvocation}' or 'help' to continue`;
        if (this.isNewSession()) {
            this.ask(`Welcome to ${skillName} skill. ${speech}`,
                reprompt);
        } else {
            const invocations = [
                'read home feed',
                'read all feed',
                'read popular feed',
                'read top posts',
                'read rising posts',
                'read new posts',
                'read controversial posts'
            ];
            const invocationProposal = getRandomElem(invocations);
            console.log(invocationProposal);
            this.ask(`Lets start from the begining, for example, you can say ${invocationProposal} to continue`,
                `Say '${invocationProposal}' or 'help' to continue`);
        }
    },

    async ReadFeed() {
        console.log('ReedFeed intent call');
        if (!hasAccess.call(this)) {return}
        console.log(getTimeDifference.call(this));
        console.log(this.$inputs);
        let feed = (this.$inputs.feed) ? this.$inputs.feed.value: null;
        let sort = (this.$inputs.sort) ? this.$inputs.sort.value: null;
        await getFeedData.call(this, sort, feed);
        await readFeedTitles.call(this, 0);
    },

    'ReadingPostsState': {
        async OpenThis() {
            console.log(getTimeDifference.call(this));
            const postsInfo = this.$session.$data.posts;
            const { startTime, posts } = await userDb.readData(this.getUserId());
            const interval = Date.now() - startTime;
            console.log(`interval is: ${interval}`);

            let index = -1;
            do {
                index++;
                console.log(`index is: ${index}`);
                console.log(postsInfo[index].time, interval);

            } while (index < postsInfo.length - 1 && postsInfo[index].time < interval);

            const selectedPostIndex = postsInfo[index].index;

            const selectedPost = posts[selectedPostIndex];
            userDb.writeData(this.getUserId(), {index: selectedPostIndex + 1});

            console.log(`post index from range 0-4 : ${index}`);
            console.log(`selected post index is: ${selectedPostIndex}`);
            await readPost.call(this, selectedPost);
        },

        async OpenNumber() {
            const index = parseInt(this.$inputs.postNumber.id);
            console.log(`index is: ${index}`);
            console.log(this.$inputs.postNumber);
            if (![0, 1, 2, 3, 4].includes(index)) {
                this.ask(`Sorry, i didn't get the number of post, can you specify post number from first till fifth to continue?`,
                    'Specify post number, please');
                return;
            }
            const generalIndex = this.$session.$data.posts[index].index;
            console.log(`general index: ${generalIndex}`);
            const selectedPost = (await userDb.readData(this.getUserId())).posts[generalIndex];
            userDb.writeData(this.getUserId(), {index: generalIndex + 1});
            console.log(selectedPost.selftext);
            await readPost.call(this, selectedPost);
        },

        async Yes() {
            const nextIndex = this.$session.$data.posts[4].index + 1;
            await readFeedTitles.call(this, nextIndex);
        },

        async No() {
            return this.toStatelessIntent("LAUNCH")
        },

        /*async GoHome() {
            this.followUpState(null);
            return this.toStatelessIntent("LAUNCH");
        },*/

        async Help() {
            const isAlexa = this.getType() === 'AlexaSkill';
            this.ask(`You are listening to posts feed now. 
You can say ${isAlexa ? 'alexa, open' : 'OK Google, open'}, during pause between post's titles to open specific post. 
Also, you can say 'open number', pointing out sequence number of desired post.
Eventually, you can say 'go home' to start from beginning`,
                `You can say 'open first', or 'go home', to continue`);
        },

        async Unhandled() {
            this.ask(unhandledSpeech, unhandledSpeechReprompt);
        },

        'OpenedPostState': {
            async BackToFeed() {
                this.followUpState('ReadFeed');
                await readFeedTitles.call(this);
            },

            async ReadComments() {
                const {posts, index} = await userDb.readData(this.getUserId());
                const postData = posts[index - 1];
                const postCommentsRaw = await redditApi.getCommentaries(this.getAccessToken(), postData.data.permalink);
                //console.log(postCommentsRaw);
                const actionCommentSpeech = `You can say 'back to feed' or 'go home' to continue`;
                const actionCommentSpeechReprompt = `Say 'back to feed', or 'go home'`;

                let postComments = [];
                for (let comment of postCommentsRaw) {
                    if (isEnglish(comment.text)) {
                        postComments.push(comment);
                    }
                    if (postComments.length === 5) break;
                }

                if (postComments.length === 0) {
                    this.ask(`Here's no comments for this post. ${actionCommentSpeech}`, actionCommentSpeechReprompt);
                    return;
                }
                const speechParts = [`Here is ${postComments.length} best commment${(postComments.length === 1) ? "": "s"}.`];
                for (let comment of postComments) {
                    speechParts.push(`Comment from ${comment.author} - ${cleanText(comment.text)}.`);
                }
                speechParts.push(actionCommentSpeech);
                this.ask(speechParts.join(' '), actionCommentSpeechReprompt);
            },

            async Help() {
                this.ask(`Now you are listening to post details, 
you can say 'read comments', or 'back to feed', or 'go home', to continue`,
                    `Say command, please`)
            },

            /*async GoHome() {
                this.followUpState(null);
                return this.toStatelessIntent("LAUNCH");
            },*/

            async Unhandled() {
                this.ask(unhandledSpeech, unhandledSpeechReprompt);
            }
        },

        'LongArticleState': {
            async Yes() {
                const { reading } = await userDb.readData(this.getUserId());
                this.$data.nonFirst = true;
                const { speech } = await getTextPost.call(this, reading);
                this.ask(speech, longArticleReprompt);
            },

            async No() {
                this.followUpState('ReadingPostsState.OpenedPostState');
                this.ask(returnToFeedSpeech, `Say 'read comments' or 'back to feed'`);
            },

            async BackToFeed() {
                this.followUpState('ReadFeed');
                await readFeedTitles.call(this);
            },

            /*async GoHome() {
                this.followUpState(null);
                return this.toStatelessIntent("LAUNCH");
            },*/

            'AMAZON.StopIntent'() {
                this.followUpState('ReadingPostsState.OpenedPostState');
                this.ask(`If you want to quit - say 'stop', otherwise say 'back to feed.`,
                    `Say 'stop', or 'back to feed'`);
            },

            async Unhandled() {
                this.ask(unhandledSpeech, unhandledSpeechReprompt);
            },

            async Help() {
                this.ask(`Currently you are listening to long post, 
you can say 'yes', if you want to listen more or 
'no', if you want to get more options`,
                    `Say 'yes', or 'no', or 'go home'`);
            }
        }
    },

    GoHome() {
        if (!hasAccess.call(this)) return;
        this.followUpState(null);
        return this.toIntent('LAUNCH');
    },

    'AMAZON.CancelIntent'() {
        this.tell('See you later.');
    },

    'AMAZON.StopIntent'() {
        this.tell('See you later.');
    },

    Help() {
        if (!hasAccess.call(this)) {return}
        this.ask(`This skill allows you to listen to your personal feed from reddit.
For reading your feed just say 'read my feed'. Also you can specify post sorting, 
for example, you can say 'read new posts', 
here's available sortings: best, hot, new, top, rising, controversial. 
You can specify feed type, for example 'read popular feed',  
here's available feed's types: home, popular, all, original. 
Also, you can combine sorting and feed type in one request, for example
you can say, 'read new posts from popular feed'. Now you can say your command.`,
            `Say your command to continue`)
    },

    async Unhandled() {
        this.ask('Say your command again, please');
    }

});

module.exports.app = app;
